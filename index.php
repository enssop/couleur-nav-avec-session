<?php
session_start();

if ( isset($_GET['color']) && in_array($_GET['color'], ['orange', 'blue']) ) {
    $_SESSION['color'] = $_GET['color'];
}

?><!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>sessions php</title>
    <style>
    body {
        margin-top: 40px;
    }

    nav {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
    }

    .orange { background-color: orange; }
    .blue { background-color: blue; }

    </style>
</head>
<body>
    
<nav class="<?php echo isset($_SESSION['color']) ? $_SESSION['color'] : 'orange'; ?>">
    navbar
</nav>

<a href="/index.php?color=orange">orange</a> - 
<a href="/index.php?color=blue">bleu</a>

</body>
</html>

